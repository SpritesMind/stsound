include makerules

all: lib ymplayer ym2wav

lib: 
	$(MAKE) -C StSoundLibrary all
	
ymplayer:
	$(MAKE) -C SmallYmPlayer all

ym2wav:
	$(MAKE) -C Ym2Wav all

clean:
	$(MAKE) -C StSoundLibrary clean
	$(MAKE) -C SmallYmPlayer clean
	$(MAKE) -C Ym2Wav clean
	
	
.PHONY: all clean ymplayer ym2wav